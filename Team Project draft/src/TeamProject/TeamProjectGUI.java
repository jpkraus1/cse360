package TeamProject;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import swing2swt.layout.FlowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Table;

public class TeamProjectGUI {
	
	private Composite welcomeScreen;
	private Composite manualOrFileScreen;
	private Composite manualScreen;
	private Composite editLetterCutoffsScreen;
	private Composite editGradesScreen;
	private Composite fileScreen;
	private Composite AnalyticsScreen;
	
	protected Shell shlGradeAnalytics;
	private Text textMinWelcome;
	private Text textMaxWelcome;
	private Text txtEntryManual;
	private Text textChosenFile;
	private Text textAddGradeEdit;
	private Text textReplaceWithEdit;
	private Text textACutoff;
	private Text textBCuttoff;
	private Text textCCutoff;
	private Text textDCutoff;
	private Text textECutoff;
	private Label lblMedianAnalytics;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			TeamProjectGUI window = new TeamProjectGUI();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlGradeAnalytics.open();
		shlGradeAnalytics.layout();
		while (!shlGradeAnalytics.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlGradeAnalytics = new Shell();
		shlGradeAnalytics.setSize(742, 596);
		shlGradeAnalytics.setText("Grade Analytics");
		shlGradeAnalytics.setLayout(null);
		
		//Composites for each screen view
		Composite welcomeScreen = new Composite(shlGradeAnalytics, SWT.NONE);
		welcomeScreen.setBounds(0, 0, 0, 0);
		welcomeScreen.setVisible(true);
		
		Composite manualScreen = new Composite(shlGradeAnalytics, SWT.NONE);
		manualScreen.setBounds(0, 0, 0, 0);
		manualScreen.setVisible(false);
		
		Composite editLetterCutoffScreen = new Composite(shlGradeAnalytics, SWT.NONE);
		editLetterCutoffScreen.setBounds(0, 0, 0, 0);
		editLetterCutoffScreen.setVisible(false);
		
		Composite editGradesScreen = new Composite(shlGradeAnalytics, SWT.NONE);
		editGradesScreen.setBounds(0, 0, 0, 0);
		editGradesScreen.setVisible(false);
		
		Composite analyticsScreen = new Composite(shlGradeAnalytics, SWT.BORDER);
		analyticsScreen.setBounds(0, 0, 0, 0);
		analyticsScreen.setVisible(false);
		
		Composite fileScreen = new Composite(shlGradeAnalytics, SWT.NONE);
		fileScreen.setBounds(0, 0, 0, 0);
		fileScreen.setVisible(false);
		
		Composite manualOrFileScreen = new Composite(shlGradeAnalytics, SWT.NONE);
		manualOrFileScreen.setBounds(0, 0, 0, 0);
		manualOrFileScreen.setVisible(false);
		
		//WelcomeScreen
			Label lblWelcome = new Label(welcomeScreen, SWT.NONE);
			lblWelcome.setAlignment(SWT.CENTER);
			lblWelcome.setBounds(258, 48, 200, 20);
			lblWelcome.setText("Welcome To Grade Analytics");
			
			Label lblEnterMinWelcome = new Label(welcomeScreen, SWT.NONE);
			lblEnterMinWelcome.setBounds(99, 142, 212, 20);
			lblEnterMinWelcome.setText("Enter Minimum Grade Possible");
			
			Label lblEnterMaxWelcome = new Label(welcomeScreen, SWT.NONE);
			lblEnterMaxWelcome.setText("Enter Maximum Grade Possible");
			lblEnterMaxWelcome.setBounds(99, 236, 212, 20);
			
			textMinWelcome = new Text(welcomeScreen, SWT.BORDER);
			textMinWelcome.setBounds(349, 139, 78, 26);
			
			textMaxWelcome = new Text(welcomeScreen, SWT.BORDER);
			textMaxWelcome.setBounds(349, 233, 78, 26);
			
			Button btnNextWelcome = new Button(welcomeScreen, SWT.NONE);
			btnNextWelcome.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					
				}
			});
			btnNextWelcome.setBounds(323, 474, 90, 30);
			btnNextWelcome.setText("Next");
				
		//ManualorFileScreen
			Label lblHowToEnterMorF = new Label(manualOrFileScreen, SWT.NONE);
			lblHowToEnterMorF.setAlignment(SWT.CENTER);
			lblHowToEnterMorF.setBounds(225, 88, 273, 20);
			lblHowToEnterMorF.setText("How would you like to enter grades?");
			
			Button btnFromFileMorF = new Button(manualOrFileScreen, SWT.NONE);
			btnFromFileMorF.setBounds(81, 281, 90, 30);
			btnFromFileMorF.setText("From a file");
			
			Button btnManuallyMorF = new Button(manualOrFileScreen, SWT.NONE);
			btnManuallyMorF.setBounds(537, 281, 90, 30);
			btnManuallyMorF.setText("Manually");
			
			Button btnBackMorF = new Button(manualOrFileScreen, SWT.NONE);
			btnBackMorF.setBounds(10, 509, 90, 30);
			btnBackMorF.setText("Back ");
					
				
		//ManualScreen GUI
			Label lblEnterGrades = new Label(manualScreen, SWT.NONE);
			lblEnterGrades.setAlignment(SWT.CENTER);
			lblEnterGrades.setBounds(287, 124, 149, 20);
			lblEnterGrades.setText("Enter grades below");
			
			txtEntryManual = new Text(manualScreen, SWT.BORDER);
			txtEntryManual.setBounds(317, 207, 90, 26);
			
			Button btnBackManual = new Button(manualScreen, SWT.NONE);
			btnBackManual.setBounds(10, 509, 90, 30);
			btnBackManual.setText("Back");
			
			Button btnNextManual = new Button(manualScreen, SWT.NONE);
			btnNextManual.setBounds(624, 509, 90, 30);
			btnNextManual.setText("Next");
			
			Button btnEnterGradeManual = new Button(manualScreen, SWT.NONE);
			btnEnterGradeManual.setBounds(317, 259, 90, 30);
			btnEnterGradeManual.setText("Enter Grade");
		
		
		//editLetterCutoffScreen
			Label lblACuttoff = new Label(editLetterCutoffScreen, SWT.NONE);
			lblACuttoff.setBounds(40, 93, 145, 20);
			lblACuttoff.setText("Lowest score for an A:");
			
			Label lblBCuttoff = new Label(editLetterCutoffScreen, SWT.NONE);
			lblBCuttoff.setText("Lowest score for a B:");
			lblBCuttoff.setBounds(40, 146, 145, 20);
			
			Label lblCCuttoff = new Label(editLetterCutoffScreen, SWT.NONE);
			lblCCuttoff.setText("Lowest score for a C:");
			lblCCuttoff.setBounds(40, 210, 145, 20);
			
			Label lblDCuttoff = new Label(editLetterCutoffScreen, SWT.NONE);
			lblDCuttoff.setText("Lowest score for a D:");
			lblDCuttoff.setBounds(40, 281, 145, 20);
			
			Label lblECuttoff = new Label(editLetterCutoffScreen, SWT.NONE);
			lblECuttoff.setText("Lowest score for an E:");
			lblECuttoff.setBounds(40, 349, 145, 20);
			
			textACutoff = new Text(editLetterCutoffScreen, SWT.BORDER);
			textACutoff.setBounds(317, 87, 78, 26);
			
			textBCuttoff = new Text(editLetterCutoffScreen, SWT.BORDER);
			textBCuttoff.setBounds(317, 140, 78, 26);
			
			textCCutoff = new Text(editLetterCutoffScreen, SWT.BORDER);
			textCCutoff.setBounds(317, 204, 78, 26);
			
			textDCutoff = new Text(editLetterCutoffScreen, SWT.BORDER);
			textDCutoff.setBounds(317, 275, 78, 26);
			
			textECutoff = new Text(editLetterCutoffScreen, SWT.BORDER);
			textECutoff.setBounds(317, 343, 78, 26);
			
			Button btnEnterCutoff = new Button(editLetterCutoffScreen, SWT.NONE);
			btnEnterCutoff.setBounds(624, 509, 90, 30);
			btnEnterCutoff.setText("Enter");
			
			Button btnBackCutoff = new Button(editLetterCutoffScreen, SWT.NONE);
			btnBackCutoff.setBounds(10, 509, 90, 30);
			btnBackCutoff.setText("Back");
			
		
		//editGradesScreen GUI
			Label lblAddGradeEdit = new Label(editGradesScreen, SWT.NONE);
			lblAddGradeEdit.setBounds(10, 55, 94, 20);
			lblAddGradeEdit.setText("Add a Grade");
			
			textAddGradeEdit = new Text(editGradesScreen, SWT.BORDER);
			textAddGradeEdit.setBounds(196, 52, 97, 26);
			
			Button btnAddGradeEdit = new Button(editGradesScreen, SWT.NONE);
			btnAddGradeEdit.setBounds(547, 50, 90, 30);
			btnAddGradeEdit.setText("Enter");
			
			Combo comboDeleteGradeEdit = new Combo(editGradesScreen, SWT.NONE);
			comboDeleteGradeEdit.setBounds(196, 144, 97, 28);
			
			Label lblDeleteGradeEdit = new Label(editGradesScreen, SWT.NONE);
			lblDeleteGradeEdit.setBounds(10, 147, 105, 20);
			lblDeleteGradeEdit.setText("Delete a Grade");
			
			Button btnDeleteGradeEdit = new Button(editGradesScreen, SWT.NONE);
			btnDeleteGradeEdit.setBounds(547, 142, 90, 30);
			btnDeleteGradeEdit.setText("Enter");
			
			Label lblSwapGradeEdit = new Label(editGradesScreen, SWT.NONE);
			lblSwapGradeEdit.setBounds(10, 260, 161, 20);
			lblSwapGradeEdit.setText("Choose a Grade to swap");
			
			Combo comboSwapGradeEdit = new Combo(editGradesScreen, SWT.NONE);
			comboSwapGradeEdit.setBounds(196, 257, 97, 28);
			
			Label lblReplaceItWithEdit = new Label(editGradesScreen, SWT.NONE);
			lblReplaceItWithEdit.setBounds(10, 316, 113, 20);
			lblReplaceItWithEdit.setText("Replace it with");
			
			textReplaceWithEdit = new Text(editGradesScreen, SWT.BORDER);
			textReplaceWithEdit.setBounds(196, 310, 78, 26);
			
			Button btnSwapGradeEdit = new Button(editGradesScreen, SWT.NONE);
			btnSwapGradeEdit.setBounds(547, 285, 90, 30);
			btnSwapGradeEdit.setText("Enter");
			
			Button btnBackEdit = new Button(editGradesScreen, SWT.NONE);
			btnBackEdit.setBounds(10, 509, 90, 30);
			btnBackEdit.setText("Back");
			
			Label dividerDelete_SwapEdit = new Label(editGradesScreen, SWT.SEPARATOR | SWT.HORIZONTAL);
			dividerDelete_SwapEdit.setBounds(0, 220, 724, 2);
			
			Label dividerAdd_DeleteEdit = new Label(editGradesScreen, SWT.SEPARATOR | SWT.HORIZONTAL);
			dividerAdd_DeleteEdit.setBounds(0, 114, 724, 2);
		
		//analytics Screen GUI
			Label lblLowestGradeAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblLowestGradeAnalytics.setBounds(30, 28, 97, 20);
			lblLowestGradeAnalytics.setText("Lowest Grade");
			
			Label lblHighestGradeAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblHighestGradeAnalytics.setBounds(30, 80, 122, 20);
			lblHighestGradeAnalytics.setText("Highest Grade");
			
			Label lblAverageAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblAverageAnalytics.setBounds(30, 133, 70, 20);
			lblAverageAnalytics.setText("Average");
			
			lblMedianAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblMedianAnalytics.setBounds(30, 189, 70, 20);
			lblMedianAnalytics.setText("Median");
			
			Label textLowestGradeAnalytics = new Label(analyticsScreen, SWT.BORDER);
			textLowestGradeAnalytics.setBounds(172, 28, 97, 20);
			
			Label textHighestGradeAnalytics = new Label(analyticsScreen, SWT.BORDER);
			textHighestGradeAnalytics.setBounds(172, 80, 97, 20);
			
			Label texAverageGradeAnalytics = new Label(analyticsScreen, SWT.BORDER);
			texAverageGradeAnalytics.setBounds(172, 132, 97, 20);
			
			Label textMedianLabelAnalytics = new Label(analyticsScreen, SWT.BORDER);
			textMedianLabelAnalytics.setBounds(172, 189, 97, 20);
			
			Label lblPercentilesAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblPercentilesAnalytics.setBounds(289, 28, 217, 20);
			lblPercentilesAnalytics.setText("Percentiles based off high score");
			
			Combo comboChoosePercentilesAnalytics = new Combo(analyticsScreen, SWT.NONE);
			comboChoosePercentilesAnalytics.setItems(new String[] {"100%", "90%", "80%", "70%", "60%", "50%", "40%", "30%", "20%", "10%"});
			comboChoosePercentilesAnalytics.setBounds(567, 25, 143, 49);
			
			Label lblRangeOfScoresAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblRangeOfScoresAnalytics.setBounds(288, 80, 256, 20);
			lblRangeOfScoresAnalytics.setText("Range of scores within this percentile");
			
			Label rangeOfPercentile = new Label(analyticsScreen, SWT.BORDER);
			rangeOfPercentile.setBounds(567, 79, 143, 20);
			
			Label lblLetterDistAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblLetterDistAnalytics.setBounds(289, 259, 180, 20);
			lblLetterDistAnalytics.setText("Letter Grade Distribution");
			
			Combo comboChooseLetterAnalytics = new Combo(analyticsScreen, SWT.NONE);
			comboChooseLetterAnalytics.setItems(new String[] {"A", "B", "C", "D", "E"});
			comboChooseLetterAnalytics.setBounds(567, 256, 141, 28);
			
			Label lblThereAreAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblThereAreAnalytics.setBounds(286, 320, 70, 20);
			lblThereAreAnalytics.setText("There are");
			
			Label numberOfThisGradeAnalytics = new Label(analyticsScreen, SWT.BORDER);
			numberOfThisGradeAnalytics.setBounds(362, 319, 155, 20);
			
			Label lblOfTheChosenAnalytics = new Label(analyticsScreen, SWT.NONE);
			lblOfTheChosenAnalytics.setBounds(530, 320, 180, 20);
			lblOfTheChosenAnalytics.setText("of the chosen letter grade");
			
			Button btnBackAnalytics = new Button(analyticsScreen, SWT.NONE);
			btnBackAnalytics.setBounds(10, 505, 90, 30);
			btnBackAnalytics.setText("Back");
			
			Button btnSaveToFileAnalytics = new Button(analyticsScreen, SWT.NONE);
			btnSaveToFileAnalytics.setBounds(620, 505, 90, 30);
			btnSaveToFileAnalytics.setText("Save to file");
			
			Button btnEditGradesAnalytics = new Button(analyticsScreen, SWT.NONE);
			btnEditGradesAnalytics.setBounds(76, 425, 106, 30);
			btnEditGradesAnalytics.setText("Edit Grades");
			
			Button btnEditLetterGradeAnalytics = new Button(analyticsScreen, SWT.NONE);
			btnEditLetterGradeAnalytics.setBounds(425, 425, 180, 30);
			btnEditLetterGradeAnalytics.setText("Edit Letter Grade Cutoffs");
			
			Label dividerVertAnalytics = new Label(analyticsScreen, SWT.SEPARATOR | SWT.VERTICAL);
			dividerVertAnalytics.setBounds(275, 0, 5, 545);
			
			Label dividerHorizAnalytics = new Label(analyticsScreen, SWT.BORDER | SWT.SEPARATOR | SWT.HORIZONTAL);
			dividerHorizAnalytics.setBounds(275, 171, 445, 2);
			
		
		//File Screen GUI
			textChosenFile = new Text(fileScreen, SWT.BORDER | SWT.READ_ONLY);
			textChosenFile.setBounds(0, 0, 724, 369);
			
			Button btnBackFile = new Button(fileScreen, SWT.NONE);
			btnBackFile.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
				}
			});
			btnBackFile.setBounds(10, 509, 90, 30);
			btnBackFile.setText("Back");
			
			Button btnChooseFile = new Button(fileScreen, SWT.NONE);
			btnChooseFile.setBounds(317, 389, 90, 30);
			btnChooseFile.setText("Choose a file");
			
			Button btnNextFile = new Button(fileScreen, SWT.NONE);
			btnNextFile.setBounds(624, 509, 90, 30);
			btnNextFile.setText("Next");
		

	}
}
