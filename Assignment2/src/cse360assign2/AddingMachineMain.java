package cse360assign2;
import java.util.Scanner;

public class AddingMachineMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		AddingMachine adder = new AddingMachine();
		int selection = 0;
		
		while(selection != 6) {
			
			System.out.println("1. Add \n" + 
					"2. Sub \n" + 
					"3. GetTotal \n" + 
					"4. GetHistory \n" + 
					"5. Clear \n" +
					"6. Exit");
			
			selection = in.nextInt();
			
			switch(selection) {
			case 1:
				System.out.println("What to add?");
				adder.add(in.nextInt());
				break;
			case 2:
				System.out.println("What to sub?");
				adder.subtract(in.nextInt());
				break;
			case 3:
				System.out.println("Total: " + adder.getTotal());
				break;
			case 4: 
				System.out.println("History: " + adder);
				break;
			case 5:
				System.out.println("Cleared");
				adder.clear();
				break;
			}
			
			}
		
	}

}
