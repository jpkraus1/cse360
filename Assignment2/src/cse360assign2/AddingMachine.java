package cse360assign2;

/** 
 * 
 * @author Jared Krause
 * Assignment 2
 * 
 * Original version of the Adding Machine class
 */
public class AddingMachine {
	
	private static final String RESET_STRING = "0";
	private final int RESET_TOTAL = 0;
	private int total;
	private String runningTotal;
	
	/** 
	 * Constructor method, initializes all variables to zero
	 */
	public AddingMachine () {
		total = RESET_TOTAL; 
		runningTotal = "0 ";
	}
	
	/**
	 * 
	 * @return Current total of the adding machine
	 */
	public int getTotal () {
		return total;
	}
	
	/**
	 *  Adds the value passed by the method
	 *  to the running total
	 * @param value
	 *  The value to be added
	 */
	public void add (int value) {
		total += value;
		runningTotal += " + " + value;
	}
	
	/** 
	 *Subtracts the value passed by the method
	 * from the running total 
	 * @param value
	 *  The value to be subtracted
	 */
	public void subtract (int value) {
		total -= value;
		runningTotal += " - " + value;
	}
	
	
	/**
	 * @return The string form of the value stored
	 *  in the adding machine
	 */
	public String toString() {
		return runningTotal;
	}
	
	/**
	 * Clears out the contents of the machine 
	 * and resets all values to zero
	 */
	public void clear() {
		total = RESET_TOTAL;
		runningTotal = RESET_STRING;
	}
				
}