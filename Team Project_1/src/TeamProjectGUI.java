/* Class that contains all GUI elements and main method.
	CSE 360 Summer 2019
	@author: Team 6
	Team 6 members: Jared Krause, Noor Attiah, Larissa Pokam
*/

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;

import java.awt.CardLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSeparator;
import java.awt.Color;
import java.awt.Container;

public class TeamProjectGUI {
		//Variables to specify which window to return to 
		//When back button pressed
		private boolean manOrFile;
		private int backToAnalytics = 0;
		
		boolean isGrades = false;
		double minGrade;
		double maxGrade;
		int numGrades = 10000;
	
		Heap gradeHeap = new Heap(10000);
		GradeAnalFunctions allFunction = new GradeAnalFunctions();
		
		//Forward declaration of GUI elements
		//Frame
		private JFrame frame;
		
		//Panels
		private JPanel panelWelcome;
		private JPanel panelGroupManOrFile;
	 	private JPanel panelManOrFile;
		private JPanel panelManual;
		private JPanel panelFile;
		private JPanel panelGroupAnalytics;
		private JPanel panelAnalytics;
		private JPanel panelEditGrades;
		private JPanel panelLetterCutoffs;
	    private JPanel panelSaveReport;
				
	    
	    //Textfields
		private JTextField textFieldMin;
		private JTextField textFieldMax;
		private JTextField textFieldNumGrades;
		private JTextField textFieldManEntry;
		private JTextField textFieldAddGrade;
		private JTextField textFieldEdit;
		private JTextField textFieldAcut;
		private JTextField textFieldBcut;
		private JTextField textFieldCcut;
		private JTextField textFieldDcut;
		private JTextField textFieldReportName;
		private JTextField textFieldFileName;
		
		//Labels
		private JLabel lblLowestGrade;
		private JLabel lblHighestGrade;
		private JLabel lblAvgGrade;
		private JLabel lblMedianGrade;
		private JLabel lblRangeofGrades;		
		private JLabel lblNumGrades;
		private JLabel lblGradeCardinality;
		
		
		//ComboBoxes
		private JComboBox cBoxPercentile;
		private JComboBox cBoxDistrib;
		private JComboBox<String> cBoxEdit;
		private JComboBox cBoxDelete;
		
		//Buttons
		private JButton btnNextPan1;
		private JButton btnNext;
		private JButton btnBack;
		private JButton btnManually;
		private JButton btnInputAFile;
		private JButton btnBackToPan1;
		private JButton btnNextGrade;
		private JButton btnBackToPan2;
		private JButton btnConfirmGrades;
		private JButton btnBackToPan2_1;
		private JButton btnConfirmFile;
		private JButton btnEditGrades;
		private JButton btnEditLetterGrade; 
		private JButton btnBackToPan2ab;
		private JButton btnSaveReport;
		private JButton btnConfirmAdd;
		private JButton btnConfirmEdit;
		private JButton btnDelete;
		private JButton btnFinishAndSave;
		private JButton btnBackToPan3;
		private JButton btnBackToPan3_1;
		private JButton btnBackToPan3ab;
		private JButton btnFinishAndSave_1;
		private JButton btnFinish;
		private JButton btnConfirmCutoffs;
		
		
		private JLabel lblEnterAValid;
		private JLabel lblEnteraFile;
		private JLabel lblEnterAValid_1;
		private JButton btnFinishWithoutSaving;
		
		

		
		
		/**
		 * Launch the application.
		 */
		public static void main(String[] args) {
			
					try {
						TeamProjectGUI window = new TeamProjectGUI();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
			
		}
	
		/**
		 * Create the application.
		 */
		public TeamProjectGUI() {
			initialize();
		}
	
		/**
		 * Initialize the contents of the frame.
		 */
		private void initialize() {
			
			
			
			frame = new JFrame();
			frame.setBounds(100, 100, 741, 526);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(new CardLayout(0, 0));
	
			
		//Panels	
			panelWelcome = new JPanel();
			frame.getContentPane().add(panelWelcome, "Welcome");
			panelWelcome.setLayout(null);
			panelWelcome.setVisible(true);
			
			panelGroupManOrFile = new JPanel();
			frame.getContentPane().add(panelGroupManOrFile, "Manual Or File Group");
			panelGroupManOrFile.setLayout(new CardLayout(0, 0));
			panelGroupManOrFile.setVisible(false);
			
			panelManOrFile = new JPanel();
			panelGroupManOrFile.add(panelManOrFile, "Manual Or File");
			panelManOrFile.setVisible(false);
			
			panelManual = new JPanel();
			panelGroupManOrFile.add(panelManual, "Manual Entry");
			panelManual.setVisible(false);
			
			panelFile = new JPanel();
			panelGroupManOrFile.add(panelFile, "File Entry");
			panelFile.setVisible(false);
			
			panelGroupAnalytics = new JPanel();
			frame.getContentPane().add(panelGroupAnalytics, "Analytics Group");
			panelGroupAnalytics.setLayout(new CardLayout(0, 0));
			panelGroupAnalytics.setVisible(false);
			
			panelAnalytics = new JPanel();
			panelGroupAnalytics.add(panelAnalytics, "Analytics");
			panelAnalytics.setVisible(false);
			
			panelEditGrades = new JPanel();
			panelGroupAnalytics.add(panelEditGrades, "Edit Grades");
			panelEditGrades.setVisible(false);
			
			panelLetterCutoffs = new JPanel();
			panelGroupAnalytics.add(panelLetterCutoffs, "Letter Cutoffs");
			panelLetterCutoffs.setVisible(false);
			
			panelSaveReport = new JPanel();
			frame.getContentPane().add(panelSaveReport, "Save Report");
			panelSaveReport.setVisible(false);
			
			
		//Welcome panel	
			//Labels
			JLabel lblWelcome = new JLabel("Welcome to Grade Analytics");
			lblWelcome.setBounds(211, 7, 298, 29);
			lblWelcome.setVerticalAlignment(SwingConstants.BOTTOM);
			lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
			panelWelcome.add(lblWelcome);
			lblWelcome.setFont(new Font("Tahoma", Font.PLAIN, 24));
			
			JLabel lblMinWelcome = new JLabel("Enter Minimum Grade: ");
			lblMinWelcome.setBounds(125, 98, 166, 20);
			lblMinWelcome.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelWelcome.add(lblMinWelcome);
			
			JLabel lblMaxWelcome = new JLabel("Enter Maximum Grade:");
			lblMaxWelcome.setBounds(127, 267, 164, 20);
			lblMaxWelcome.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelWelcome.add(lblMaxWelcome);
			
			//Button
			btnNextPan1 = new JButton("Next");
			btnNextPan1.setBounds(326, 352, 110, 33);
			btnNextPan1.setFont(new Font("Tahoma", Font.PLAIN, 20));
			panelWelcome.add(btnNextPan1);
			btnNextPan1.addActionListener(new ScreenButton());
				//If next is pressed, go to next screen
			
			//TextFields
			textFieldMin = new JTextField();
			textFieldMin.setBounds(437, 98, 138, 22);
			textFieldMin.setText("");
			panelWelcome.add(textFieldMin);
			textFieldMin.setColumns(10);
			
			textFieldMax = new JTextField();
			textFieldMax.setBounds(437, 267, 138, 22);
			panelWelcome.add(textFieldMax);
			textFieldMax.setColumns(10);
			
			lblEnterAValid = new JLabel("Enter a valid minimum and maximum number!");
			lblEnterAValid.setVisible(false);
			lblEnterAValid.setForeground(Color.RED);
			lblEnterAValid.setHorizontalAlignment(SwingConstants.CENTER);
			lblEnterAValid.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblEnterAValid.setBounds(137, 312, 402, 16);
			panelWelcome.add(lblEnterAValid);
			panelManOrFile.setLayout(null);
			
			
			
		//Manual or file panel	
			//Labels
			JLabel lblManOrFile = new JLabel("How would you like to input Grades?");
			lblManOrFile.setBounds(188, 93, 326, 25);
			lblManOrFile.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblManOrFile.setHorizontalAlignment(SwingConstants.CENTER);
			panelManOrFile.add(lblManOrFile);
			
			//Buttons
			btnManually = new JButton("Manually");
			btnManually.setBounds(89, 296, 113, 29);	
			btnManually.addActionListener(new ScreenButton()); 
			btnManually.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelManOrFile.add(btnManually);
				//If "Manually" button is pressed, go to manual screen
			
			btnInputAFile = new JButton("Input a File");
			btnInputAFile.setBounds(518, 296, 124, 29);
			btnInputAFile.addActionListener(new ScreenButton());
			btnInputAFile.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelManOrFile.add(btnInputAFile);
				//If "File" button is pressed, advance to the file screen
			
			btnBackToPan1 = new JButton("Back");
			btnBackToPan1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnBackToPan1.addActionListener(new ScreenButton());
			btnBackToPan1.setBounds(87, 414, 115, 25);
			panelManOrFile.add(btnBackToPan1);
			panelManual.setLayout(null);
				//Back to welcome page
			
		//Panel for manual grade entry	
			
			//Labels
			JLabel lblEnterGrades = new JLabel("Enter Grades:");
			lblEnterGrades.setBounds(33, 224, 97, 20);
			lblEnterGrades.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelManual.add(lblEnterGrades);
			
			JLabel lblEnterFileName = new JLabel("Enter File name");
			lblEnterFileName.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblEnterFileName.setBounds(113, 229, 119, 16);
			panelFile.add(lblEnterFileName);
			
			
			//Buttons
			btnNextGrade = new JButton("Next Grade");
			btnNextGrade.setBounds(421, 220, 133, 29);
			btnNextGrade.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnNextGrade.addActionListener(new ScreenButton());
			panelManual.add(btnNextGrade);
				//Enters the current grade and prepares for input of the next
			
			btnBackToPan2 = new JButton("Back");
			btnBackToPan2.setBounds(139, 388, 85, 29);
			btnBackToPan2.addActionListener(new ScreenButton());
			btnBackToPan2.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelManual.add(btnBackToPan2);
				//If "Back" is pressed, go back to previous screen
			
			btnConfirmGrades = new JButton("Confirm Grades");
			btnConfirmGrades.setBounds(506, 388, 158, 29);
			btnConfirmGrades.addActionListener(new ScreenButton());
			btnConfirmGrades.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelManual.add(btnConfirmGrades);
				//Confirms that all grades have been entered and continues to next screen
				
			btnBackToPan2_1 = new JButton("Back");
			btnBackToPan2_1.setBounds(31, 414, 119, 29);
			btnBackToPan2_1.addActionListener(new ScreenButton());
			btnBackToPan2_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelFile.add(btnBackToPan2_1);
				//If back pressed, go to previous screen
			
			btnConfirmFile = new JButton("Confirm File");
			btnConfirmFile.setBounds(548, 414, 151, 29);
			btnConfirmFile.addActionListener(new ScreenButton());			
			btnConfirmFile.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelFile.add(btnConfirmFile);
				//Once file is confirmed, go to analytics page
			
			//TextFields
			textFieldManEntry = new JTextField();
			textFieldManEntry.setBounds(169, 224, 85, 22);
			panelManual.add(textFieldManEntry);
			textFieldManEntry.setColumns(10);
			
			lblEnterAValid_1 = new JLabel("Enter a valid number between the min and max grades!");
			lblEnterAValid_1.setVisible(false);
			lblEnterAValid_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblEnterAValid_1.setForeground(Color.RED);
			lblEnterAValid_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblEnterAValid_1.setBounds(149, 301, 405, 29);
			panelManual.add(lblEnterAValid_1);
			panelFile.setLayout(null);
			
			textFieldFileName = new JTextField();
			textFieldFileName.setBounds(367, 227, 190, 22);
			panelFile.add(textFieldFileName);
			textFieldFileName.setColumns(10);
			
			lblEnteraFile = new JLabel("Enter a file with valid grades!");
			lblEnteraFile.setVisible(false);
			lblEnteraFile.setForeground(Color.RED);
			lblEnteraFile.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblEnteraFile.setBounds(250, 308, 307, 22);
			panelFile.add(lblEnteraFile);
					
		//Analytics panel	
			panelAnalytics.setLayout(null);
			
			//ComboBoxes
			cBoxPercentile = new JComboBox();
			cBoxPercentile.setBounds(327, 62, 331, 26);
			cBoxPercentile.addActionListener(new BoxListener());
			cBoxPercentile.setFont(new Font("Tahoma", Font.PLAIN, 16));
			cBoxPercentile.setModel(new DefaultComboBoxModel(new String[] 
					{"0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100"}));
			panelAnalytics.add(cBoxPercentile);
			
			cBoxDistrib = new JComboBox();
			cBoxDistrib.addActionListener(new BoxListener());
			cBoxDistrib.setBounds(327, 227, 331, 26);
			cBoxDistrib.setModel(new DefaultComboBoxModel(new String[] {"A", "B", "C", "D", "F"}));
			cBoxDistrib.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(cBoxDistrib);
			
			//Labels
			JLabel lblLowestGrade_1 = new JLabel("Lowest Grade");
			lblLowestGrade_1.setBounds(55, 36, 96, 20);
			lblLowestGrade_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblLowestGrade_1);
			
			JLabel lblPercentilesBasedOff = new JLabel("Percentiles Based off high score");
			lblPercentilesBasedOff.setBounds(327, 36, 223, 20);
			lblPercentilesBasedOff.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblPercentilesBasedOff);
			
			JLabel lblHighestGrade_1 = new JLabel("Highest Grade");
			lblHighestGrade_1.setBounds(51, 92, 100, 20);
			lblHighestGrade_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblHighestGrade_1);
			
			JLabel lblRangeOfScores = new JLabel("Range of Scores within this percentile");
			lblRangeOfScores.setBounds(327, 118, 265, 20);
			lblRangeOfScores.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblRangeOfScores);
			
			JLabel lblAverageGrade = new JLabel("Average Grade");
			lblAverageGrade.setBounds(46, 145, 105, 20);
			lblAverageGrade.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblAverageGrade);
			
			JLabel lblMedianGrade_1 = new JLabel("Median Grade");
			lblMedianGrade_1.setBounds(54, 201, 97, 20);
			lblMedianGrade_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblMedianGrade_1);
			
			JLabel lblDistributionOfLetter = new JLabel("Distribution of letter Grades");
			lblDistributionOfLetter.setBounds(327, 201, 196, 20);
			lblDistributionOfLetter.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblDistributionOfLetter);
			
			JLabel lblNumberOfTimes = new JLabel("Number of times this grade was received:");
			lblNumberOfTimes.setBounds(327, 286, 295, 20);
			lblNumberOfTimes.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(lblNumberOfTimes);
		
			//Buttons		
			btnEditGrades = new JButton("Edit Grades");
			btnEditGrades.addActionListener(new ScreenButton());
			btnEditGrades.setBounds(38, 369, 145, 29);
			btnEditGrades.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(btnEditGrades);
				//Takes the user to the Grade editing screen
			
			btnEditLetterGrade = new JButton("Edit Letter Grade Cutoffs");
			btnEditLetterGrade.addActionListener(new ScreenButton());
			btnEditLetterGrade.setBounds(426, 369, 232, 29);
			btnEditLetterGrade.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(btnEditLetterGrade);
				//Takes the user to the letter grade cutoff screen
			
			btnBackToPan2ab = new JButton("Back");
			btnBackToPan2ab.addActionListener(new ScreenButton());
			btnBackToPan2ab.setBounds(55, 431, 87, 29);
			btnBackToPan2ab.setFont(new Font("Tahoma", Font.PLAIN, 16));
			//panelAnalytics.add(btnBackToPan2ab);
				//If the user input manually, takes them to manual input
				//Otherwise back to file input
			
			btnSaveReport = new JButton("Finish and Save Report");
			btnSaveReport.addActionListener(new ScreenButton());
			btnSaveReport.setBounds(426, 431, 232, 29);
			btnSaveReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelAnalytics.add(btnSaveReport);
				//Takes the user to the save as text file screen
			
			//TextFields
			lblLowestGrade = new JLabel();
			lblLowestGrade.setBounds(200, 36, 82, 22);
			panelAnalytics.add(lblLowestGrade);
			//lblLowestGrade.setColumns(10);
			
			lblHighestGrade = new JLabel();
			lblHighestGrade.setBounds(200, 92, 82, 22);
			panelAnalytics.add(lblHighestGrade);
			//lblHighestGrade.setColumns(10);
					
			lblAvgGrade = new JLabel();
			lblAvgGrade.setBounds(200, 145, 82, 22);
			panelAnalytics.add(lblAvgGrade);
			
			lblMedianGrade = new JLabel();
			lblMedianGrade.setBounds(200, 201, 82, 22);
			panelAnalytics.add(lblMedianGrade);
			
			lblRangeofGrades = new JLabel("");
			lblRangeofGrades.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblRangeofGrades.setBounds(322, 148, 336, 29);
			panelAnalytics.add(lblRangeofGrades);
			
			lblGradeCardinality = new JLabel("");
			lblGradeCardinality.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblGradeCardinality.setBounds(327, 323, 331, 26);
			panelAnalytics.add(lblGradeCardinality);
			panelEditGrades.setLayout(null);
			
		//Edit grades panel	
			
			//ComboBoxes
			cBoxEdit = new JComboBox();
			//Add the heap into the combobox
				//for(int index = 0; index < gradeHeap.getHeap().length; index++) {
				//	cBoxEdit.addItem(String.valueOf(gradeHeap.getHeap()[index]));
				//}
			cBoxEdit.addActionListener(new BoxListener());
			cBoxEdit.setBounds(160, 200, 91, 22);
			panelEditGrades.add(cBoxEdit);
			
			
			cBoxDelete = new JComboBox();
			//Add the heap into the combobox
				//for(int index = 0; index < gradeHeap.getHeap().length; index++) {
				//	cBoxDelete.addItem(String.valueOf(gradeHeap.getHeap()[index]));
				//}
			cBoxDelete.addActionListener(new BoxListener());
			cBoxDelete.setBounds(160, 337, 91, 22);
			panelEditGrades.add(cBoxDelete);
			
			
			//Labels
			JLabel lblAddAGrade = new JLabel("Add a grade");
			lblAddAGrade.setBounds(31, 62, 87, 20);
			lblAddAGrade.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelEditGrades.add(lblAddAGrade);
			
			JLabel lblEditAGrade = new JLabel("Edit a Grade");
			lblEditAGrade.setBounds(31, 200, 87, 20);
			lblEditAGrade.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelEditGrades.add(lblEditAGrade);
			
			JLabel lblChangeItTo = new JLabel("Change it to");
			lblChangeItTo.setBounds(276, 200, 86, 20);
			lblChangeItTo.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelEditGrades.add(lblChangeItTo);
			
			JLabel lblDeleteAGrade = new JLabel("Delete a Grade");
			lblDeleteAGrade.setBounds(31, 337, 104, 20);
			lblDeleteAGrade.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelEditGrades.add(lblDeleteAGrade);
			
			//Buttons
			btnConfirmAdd = new JButton("Confirm Add");
			btnConfirmAdd.setBounds(276, 58, 149, 29);
			btnConfirmAdd.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnConfirmAdd.addActionListener(new ScreenButton());
			panelEditGrades.add(btnConfirmAdd);
				//Confirms this addition, allows the next to be entered
			
			btnConfirmEdit = new JButton("Confirm Edit");
			btnConfirmEdit.setBounds(521, 196, 141, 29);
			btnConfirmEdit.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnConfirmEdit.addActionListener(new ScreenButton());
			panelEditGrades.add(btnConfirmEdit);
				//Confirms this swap, allows the next to be entered
			
			btnDelete = new JButton("Confirm Delete");
			btnDelete.setBounds(276, 333, 171, 29);
			btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnDelete.addActionListener(new ScreenButton());
			panelEditGrades.add(btnDelete);
				//Confirms this delete
				
			btnBackToPan3 = new JButton("Back");
			btnBackToPan3.addActionListener(new ScreenButton());
			btnBackToPan3.setBounds(31, 417, 87, 29);
			btnBackToPan3.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelEditGrades.add(btnBackToPan3);
				//Takes the user back to the Analytics Screen
			
			btnFinishAndSave = new JButton("Finish and Save Report");
			btnFinishAndSave.addActionListener(new ScreenButton());
			btnFinishAndSave.setBounds(479, 417, 237, 29);
			btnFinishAndSave.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelEditGrades.add(btnFinishAndSave);
				//Takes the user to the final screen
			
			//TextFields
			textFieldAddGrade = new JTextField();
			textFieldAddGrade.setBounds(160, 59, 91, 26);
			textFieldAddGrade.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelEditGrades.add(textFieldAddGrade);
			textFieldAddGrade.setColumns(10);	
	
			textFieldEdit = new JTextField();
			textFieldEdit.setBounds(387, 200, 110, 22);
			panelEditGrades.add(textFieldEdit);
			textFieldEdit.setColumns(10);
			panelLetterCutoffs.setLayout(null);
			
		//Panel for changing letter grade cutoffs
		
			//Labels
			JLabel label = new JLabel("");
			label.setBounds(40, 47, 0, 0);
			panelLetterCutoffs.add(label);
			
			JLabel lblLowestScoreFor = new JLabel("Lowest Score for an A");
			lblLowestScoreFor.setBounds(162, 36, 156, 20);
			lblLowestScoreFor.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelLetterCutoffs.add(lblLowestScoreFor);
			
			JLabel lblLowestScoreFor_1 = new JLabel("Lowest Score for a B");
			lblLowestScoreFor_1.setBounds(162, 91, 145, 20);
			lblLowestScoreFor_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelLetterCutoffs.add(lblLowestScoreFor_1);
			
			JLabel lblLowestScoreFor_2 = new JLabel("Lowest Score for a C");
			lblLowestScoreFor_2.setBounds(162, 146, 146, 20);
			lblLowestScoreFor_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelLetterCutoffs.add(lblLowestScoreFor_2);
			
			JLabel lblLowestScoreFor_3 = new JLabel("Lowest Score for a D");
			lblLowestScoreFor_3.setBounds(162, 201, 147, 20);
			lblLowestScoreFor_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelLetterCutoffs.add(lblLowestScoreFor_3);
			
			
			//Buttons
			btnBackToPan3_1 = new JButton("Back");
			btnBackToPan3_1.addActionListener(new ScreenButton());
			btnBackToPan3_1.setBounds(40, 398, 100, 29);
			btnBackToPan3_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelLetterCutoffs.add(btnBackToPan3_1);
				//Takes the user back to the previous screen
			
			btnFinishAndSave_1 = new JButton("Finish and Save Report");
			btnFinishAndSave_1.addActionListener(new ScreenButton());
			btnFinishAndSave_1.setBounds(465, 398, 226, 29);
			btnFinishAndSave_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelLetterCutoffs.add(btnFinishAndSave_1);
				//Takes the user to the final screen
			
			//TextField
			textFieldAcut = new JTextField();
			textFieldAcut.setBounds(496, 36, 195, 22);
			panelLetterCutoffs.add(textFieldAcut);
			textFieldAcut.setColumns(10);
			
			textFieldBcut = new JTextField();
			textFieldBcut.setBounds(496, 91, 195, 22);
			panelLetterCutoffs.add(textFieldBcut);
			textFieldBcut.setColumns(10);
			
			textFieldCcut = new JTextField();
			textFieldCcut.setBounds(496, 146, 195, 22);
			panelLetterCutoffs.add(textFieldCcut);
			textFieldCcut.setColumns(10);
			
			textFieldDcut = new JTextField();
			textFieldDcut.setBounds(496, 201, 195, 22);
			panelLetterCutoffs.add(textFieldDcut);
			textFieldDcut.setColumns(10);
			
			btnConfirmCutoffs = new JButton("Confirm Cutoffs");
			btnConfirmCutoffs.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnConfirmCutoffs.setBounds(332, 285, 166, 25);
			panelLetterCutoffs.add(btnConfirmCutoffs);
			
			
		//Panel for saving the final report	
			panelSaveReport.setLayout(null);
			
			//Labels
			JLabel lblWhatWouldYou = new JLabel("What would you like to name the report?");
			lblWhatWouldYou.setBounds(40, 126, 290, 20);
			lblWhatWouldYou.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelSaveReport.add(lblWhatWouldYou);
			
			//Buttons
			btnBackToPan3ab = new JButton("Back");
			btnBackToPan3ab.addActionListener(new ScreenButton());
			btnBackToPan3ab.setBounds(40, 357, 84, 29);
			btnBackToPan3ab.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelSaveReport.add(btnBackToPan3ab);
				//Takes the user back to whichever screen they were just on
			
			btnFinish = new JButton("Save grades");
			btnFinish.setBounds(451, 357, 265, 29);
			btnFinish.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnFinish.addActionListener(new ScreenButton());
			panelSaveReport.add(btnFinish);
				//Exits the program and saves the textfile
			
			textFieldReportName = new JTextField();
			textFieldReportName.setBounds(363, 123, 353, 26);
			textFieldReportName.setFont(new Font("Tahoma", Font.PLAIN, 16));
			panelSaveReport.add(textFieldReportName);
			textFieldReportName.setColumns(10);
			
			btnFinishWithoutSaving = new JButton("Finish without saving");
			btnFinishWithoutSaving.addActionListener(new ScreenButton());
			btnFinishWithoutSaving.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnFinishWithoutSaving.setBounds(451, 407, 256, 29);
			panelSaveReport.add(btnFinishWithoutSaving);
			
			
		
			
		}
	
		/**
		 * ScreenButton class which acts as action listeners for all buttons
		 * 
		 */
	private class ScreenButton implements ActionListener{
	
		public void actionPerformed(ActionEvent e) {
			JButton button =  (JButton) e.getSource();
			
			if(button.equals(btnNextPan1)) {
				try {
				String minGrade = textFieldMin.getText();
				String maxGrade = textFieldMax.getText();
				
				double mindub = Double.parseDouble(minGrade);
				double maxdub = Double.parseDouble(maxGrade);
				
				if(((textFieldMin != null && textFieldMax != null) && allFunction.checkInput(minGrade) && allFunction.checkInput(maxGrade)) 
						&& mindub <= maxdub && mindub >= 0) {
					
					
					allFunction.setLowerBound(mindub);
					allFunction.setUpperBound(maxdub);
					
					panelGroupManOrFile.setVisible(true);
					panelManOrFile.setVisible(true);
					panelWelcome.setVisible(false);
				}
				else {
					lblEnterAValid.setVisible(true);
					throw new ValidInputException("");	
				}
				
				} catch (NumberFormatException | ValidInputException exeption) {
					lblEnterAValid.setVisible(true);
					//Make the label visible
					//Needs its own screen
				}
				
				
				
			}	
			
			else if(button.equals(btnBack)) {
				panelWelcome.setVisible(true);
				panelGroupManOrFile.setVisible(false);
				panelManOrFile.setVisible(true);
			}
			else if(button.equals(btnBackToPan1)) {
				panelWelcome.setVisible(true);
				panelManOrFile.setVisible(false);
				panelGroupManOrFile.setVisible(false);
			}
			else if(button.equals(btnManually)) { 
				panelManual.setVisible(true);
				panelManOrFile.setVisible(false);
				manOrFile = true;
				
				
				
				
			}
			else if(button.equals(btnInputAFile)) {
				panelFile.setVisible(true);
				panelManOrFile.setVisible(false);
				manOrFile = false;
			}
			else if(button.equals(btnConfirmFile)) {
				
				try {
				
				if(allFunction.readInput(gradeHeap, textFieldFileName.getText())) {
					//allFunction.readInput(gradeHeap, textFieldFileName.getText());
				}
				else throw new ValidInputException("Exception");
				
				}catch (ValidInputException valid) {
					//System.out.println("invalid");
				}
				
				//Sort the heap
				allFunction.sortGradeList(gradeHeap);
				
				//prepare for grade distribution
				allFunction.gradeDistribution(gradeHeap);
				
				//Set the values of the textfields
				lblLowestGrade.setText(String.valueOf((allFunction.minimunGrade(gradeHeap))));
				lblHighestGrade.setText(String.valueOf((allFunction.maximunGrade(gradeHeap))));
				lblAvgGrade.setText(String.valueOf(allFunction.computeAverage(gradeHeap)));
				lblMedianGrade.setText(String.valueOf(allFunction.median(gradeHeap)));
				
				panelGroupAnalytics.setVisible(true);
				panelAnalytics.setVisible(true);
				panelGroupManOrFile.setVisible(false);
				panelFile.setVisible(false);
			}		
			else if(button.equals(btnNextGrade)) {
				try {
					
					
					String manGrade = textFieldManEntry.getText();
					double mandub = Double.parseDouble(manGrade);
					
					if(allFunction.checkInput(manGrade) && allFunction.checkGradeRange(mandub))  {
						
						allFunction.insertGrade(gradeHeap, manGrade);
						textFieldManEntry.setText("");
					}
					else {throw new ValidInputException("");}
				
				
				}catch( ValidInputException ecxep) {
					
					lblEnterAValid_1.setVisible(true);
					btnConfirmGrades.setEnabled(false);
				}
				//btnConfirmGrades.setEnabled(true);
				isGrades = true;
			}
			else if(button.equals(btnBackToPan2)) {
				panelManOrFile.setVisible(true);
				panelManual.setVisible(false);
			}
			else if(button.equals(btnBackToPan2_1)) {
				panelManOrFile.setVisible(true);
				panelFile.setVisible(false);
			}
			else if(button.equals(btnConfirmGrades)) {
				//Add whatever is in the textbox
				//System.out.println("isGraddes" + isGrades + "ChekcInput" + allFunction.checkInput(textFieldManEntry.getText()));
				//System.out.println(allFunction.checkGradeRange(Double.parseDouble(textFieldManEntry.getText())));		
				//System.out.println(textFieldManEntry.getText());
				
				try {
					
					String manGrade = textFieldManEntry.getText();
					double mandub = Double.parseDouble(manGrade);
					
					if((isGrades == true && manGrade == null)||
							(manGrade != null && allFunction.checkInput(manGrade) && allFunction.checkGradeRange(mandub)))  {
						System.out.println("endet IF");
						allFunction.insertGrade(gradeHeap, manGrade);
						textFieldManEntry.setText("");
						
						//Sort the heap
						allFunction.sortGradeList(gradeHeap);
				
						//Set the values of the textfields
						lblLowestGrade.setText(String.valueOf((allFunction.minimunGrade(gradeHeap))));
						lblHighestGrade.setText(String.valueOf((allFunction.maximunGrade(gradeHeap))));
						lblAvgGrade.setText(String.valueOf(allFunction.computeAverage(gradeHeap)));
						lblMedianGrade.setText(String.valueOf(allFunction.median(gradeHeap)));
						
						//Prepares for grade distribution
						allFunction.gradeDistribution(gradeHeap);
						//allFunction.printNumOf();
						
						panelGroupAnalytics.setVisible(true);
						panelAnalytics.setVisible(true);
						panelManual.setVisible(false);
						panelGroupManOrFile.setVisible(false);
						//Need to implement this to also add the last grade entered
				
						isGrades = true;
					}
					
					else {throw new ValidInputException("Threw");}
		
				
				}catch( NumberFormatException num) {
					
					lblEnterAValid_1.setVisible(true);
					
				}
				catch(ValidInputException ecxep) {
				
					lblEnterAValid_1.setVisible(true);

			}
				
				
				
			}
			else if(button.equals(btnEditGrades)) {
				//Add the heap to the combobox
				
				for(int index = 1; index < gradeHeap.size; index++) {
					Grade grade = gradeHeap.gradeList[index];
					System.out.println(grade.toString());
					cBoxEdit.addItem(String.valueOf(grade.getGrade()));
					cBoxDelete.addItem(String.valueOf(grade.getGrade()));
				}
				
				
				
				
				
				panelEditGrades.setVisible(true);
				panelAnalytics.setVisible(false);
				backToAnalytics = 2;
			}
			else if(button.equals(btnEditLetterGrade)) {
				panelLetterCutoffs.setVisible(true);
				panelAnalytics.setVisible(false);
			}
			else if(button.equals(btnBackToPan2ab)) {
				//If the user has input manually, go back to manual window
				if(manOrFile == true) {
					panelManual.setVisible(true);
					panelManOrFile.setVisible(false);
					panelGroupManOrFile.setVisible(true);
					panelGroupAnalytics.setVisible(false);
					panelAnalytics.setVisible(false);
				}
				//Else go back to file input window
				else {
					panelFile.setVisible(true);
					panelManOrFile.setVisible(false);
					panelGroupManOrFile.setVisible(true);
					panelGroupAnalytics.setVisible(false);
					panelAnalytics.setVisible(false);
				}
			}
			else if(button.equals(btnSaveReport)) {
				panelSaveReport.setVisible(true);
				panelGroupAnalytics.setVisible(false);
				backToAnalytics = 1;
			}
			else if(button.equals(btnConfirmAdd)) {
				allFunction.insertGrade(gradeHeap, textFieldAddGrade.getText());
				textFieldAddGrade.setText("");
				allFunction.gradeDistribution(gradeHeap);
			}
			else if(button.equals(btnConfirmEdit)) {
				
			}
			else if(button.equals(btnDelete)) {
				
			}
			else if(button.equals(btnConfirmCutoffs)) {
				
				if(textFieldAcut.getText() != null) {
					double cutA = Double.parseDouble(textFieldAcut.getText());
					allFunction.setACutoff(cutA);
				}
				else if(textFieldBcut.getText() != null) {
					double cutB = Double.parseDouble(textFieldBcut.getText());
					allFunction.setBCutoff(cutB);
				}
				else if(textFieldCcut.getText() != null){
					double cutC = Double.parseDouble(textFieldCcut.getText());
					allFunction.setCCutoff(cutC);
				}
				else if(textFieldDcut.getText() != null){
					double cutD = Double.parseDouble(textFieldDcut.getText());
					allFunction.setDCutoff(cutD);
				}
				
				
				
				
				
				
				allFunction.gradeDistribution(gradeHeap);
				
			}
			else if(button.equals(btnBackToPan3) || button.equals(btnBackToPan3_1)) {
				panelAnalytics.setVisible(true);
				panelLetterCutoffs.setVisible(false);
				panelEditGrades.setVisible(false);
			}
			else if(button.equals(btnFinishAndSave)) {
				panelSaveReport.setVisible(true);
				panelEditGrades.setVisible(false);
			}
			else if(button.equals(btnFinishAndSave_1)) {
				panelSaveReport.setVisible(true);
				panelGroupAnalytics.setVisible(false);
			}
			else if(button.equals(btnFinish)) {
				allFunction.saveFile(gradeHeap, textFieldFileName.getText());
				
				System.exit(0);
			}
			else if(button.equals(btnFinishWithoutSaving)) {
				System.exit(0);
			}
			else if(button.equals(btnBackToPan3ab)) {
				panelSaveReport.setVisible(false);
				panelGroupAnalytics.setVisible(true);
				panelAnalytics.setVisible(true);
			
			}
		}
	}
	
	private class BoxListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			JComboBox box = (JComboBox) e.getSource();
			String text = (String) box.getSelectedItem();
			
			if(box.equals(cBoxPercentile)) {
				double percent0 = allFunction.percentile(gradeHeap,0);
				double percent10 = allFunction.percentile(gradeHeap,10);
				double percent20 = allFunction.percentile(gradeHeap,20);
				double percent30 = allFunction.percentile(gradeHeap,30);
				double percent40 = allFunction.percentile(gradeHeap,40);
				double percent50 = allFunction.percentile(gradeHeap,50);
				double percent60 = allFunction.percentile(gradeHeap,60);
				double percent70 = allFunction.percentile(gradeHeap,70);
				double percent80 = allFunction.percentile(gradeHeap,80);
				double percent90 = allFunction.percentile(gradeHeap,90);
				
				
				switch(text) {
				case "0%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent0));
					break;
				case "10%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent10));
					break;
				case "20%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent20));
					break;
				case "30%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent30));
					break;
				case "40%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent40));
					break;
				case "50%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent50));
					break;
				case "60%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent60));
					break;
				case "70%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent70));
					break;
				case "80%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent80));
					break;
				case "90%": 
					lblRangeofGrades.setText(null);
					lblRangeofGrades.setText(String.valueOf(percent90));
					break;
				
				}
			}
			else if(box.equals(cBoxDistrib)) {
				
				
				
				String numA = String.valueOf(allFunction.getNumberOfA());
				String numB = String.valueOf(allFunction.getNumberOfB());
				String numC = String.valueOf(allFunction.getNumberOfC());
				String numD = String.valueOf(allFunction.getNumberOfD());
				String numF = String.valueOf(allFunction.getNumberOfF());
				
				
				
				
				switch(text) {
				case "A": 
					lblGradeCardinality.setText(null);
					lblGradeCardinality.setText(numA);
					break;
				case "B": 
					lblGradeCardinality.setText(null);
					lblGradeCardinality.setText(numB);
					break;
				case "C": 
					lblGradeCardinality.setText(null);
					lblGradeCardinality.setText(numC);
					break;
				case "D": 
					lblGradeCardinality.setText(null);
					lblGradeCardinality.setText(numD);
					break;
				case "F": 
					lblGradeCardinality.setText(null);
					lblGradeCardinality.setText(numF);
					break;
				}
			}
			else if(box.equals(cBoxEdit)) {
				
			}
			else if(box.equals(cBoxDelete)) {
				allFunction.deleteGrade(gradeHeap, Double.parseDouble(text));
			}
			
		}
		
		
		
	}
}