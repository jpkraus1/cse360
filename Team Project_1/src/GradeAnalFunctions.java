//package gradeAnal;

/**Class that contents the grades analysis functionalities function
 *CSE 360 Summer 2019
 *@ author: Team 6
 * Team 6 members: Jared Krause, Noor Attiah, Larissa Pokam  
 */
 
import java.io.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GradeAnalFunctions {	
	
	private double lowerBound;
	private double upperBound;
	private int numberOfA;
	private int numberOfB;
	private int numberOfC;
	private int numberOfD;
	private int numberOfF;
	private double cutoffA;
	private double cutoffB;
	private double cutoffC;
	private double cutoffD;
	
	
	
	/**
	 * default constructor
	 */
	 
	public GradeAnalFunctions() {
		super();
		numberOfA = 0;
		numberOfB = 0;
		numberOfC = 0;
		numberOfD = 0;
		numberOfF = 0;	
		cutoffA = 89.5;
		cutoffB = 79.5;
		cutoffC = 69.5;
		cutoffD = 59.5;
	}
	
	
	/**
	 * @param cutoffA the cutoffA to set
	 */
	 
	public void setACutoff(double cutoffA) {
		this.cutoffA = cutoffA;
	}


	/**
	 * @param cutoffB the cutoffB to set
	 */
	 
	public void setBCutoff(double cutoffB) {
		this.cutoffB = cutoffB;
	}


	/**
	 * @param cutoffC the cutoffC to set
	 */
	 
	public void setCCutoff(double cutoffC) {
		this.cutoffC = cutoffC;
	}


	/**
	 * @param cutoffD the cutoffD to set
	 */
	 
	public void setDCutoff(double cutoffD) {
		this.cutoffD = cutoffD;
	}


	/**
	 * Function that compute the grade percentile
	 * @return number of grades for the given percentile
	 * @param percent needed
	 * @param list of grades
	 */
	
	public double percentile(Heap allGrade, double percent) {
		
		double index;
		int whole;
		double decimal;
		
		
		sortGradeList(allGrade);
		
		index = (percent / 100) * allGrade.size;
		whole = (int) index;
		decimal = index - whole;
		
		if ( decimal >= 0) {
			
			whole = whole + 1; //round up if decimal index
		}
			
		return allGrade.gradeList[whole].getGrade();			
	}
	
	
	/**
	 * Function that reads double value from a text file
	 * @return a true if the all the data were correct and false otherwise
	 * @param fileName, the file from which the user would like to read from
	 * @param list of grades
	 */
	
	boolean readInput(Heap allGrade, String fileName) {
		
		boolean sucess = true;
		double eachGrade;
		File input;
		
		int index = 1;
		BufferedReader reader = null;
		
		try {

			input = new File(fileName);
		//	String path = input.getAbsolutePath();
			reader = new BufferedReader(new FileReader(input));
			String line = reader.readLine();
			
			while( line != null) {
		
				if(allGrade.size == allGrade.capacity) {
					
					allGrade.capacity = expandCapacity(allGrade.capacity);
				}
				
				if(checkInput(line)) {
					eachGrade = Double.parseDouble(line);
					Grade grad = new Grade(eachGrade);
					allGrade.gradeList[index] = grad;
					
					allGrade.size++; 
					index++;
					
					line = reader.readLine();
				}
				else {
					sucess = false;
					line = null;
				}
			}
		}
		catch (FileNotFoundException exp) {
		///ERROR TO PRINT TO THE SCREEN
			sucess = false;
			System.out.println("Error file not found");
			//exp.printStackTrace();
		}
		catch (IOException exep) {
			sucess = false;
			System.out.println("Error reading the file");
		    exep.printStackTrace();
		}
		
		finally {
		    try {
		        if (reader != null) {
		            reader.close();
		        }
		    }
		    catch (IOException e) {
		    	System.out.println("Error reading the file2");
		    }
		}
		
		if(sucess) {
			
			int indexA = 1; 
			
			while(indexA <= allGrade.size) {
			
				boolean check = checkGradeRange(allGrade.gradeList[indexA].getGrade());
				
				if(check)
					sucess = true;
				
				else {
					sucess = false;
					indexA = allGrade.size + 2;
				}
				
				indexA++;
			}
		}
		//System.out.println("SUCCESS");
		return sucess;
	}
	
	
	/**to verify if the input is valid (numeric) before using it.
	 * @return true if input is valid and false otherwise
	 * @param input: the input value to be check
	 */
	
	boolean checkInput(String input) {
		
		boolean check = false;
		
		try {  
			
		    Double.parseDouble(input);  
		    check = true;
		 } 
		catch(NumberFormatException e){  
			
			  check = false;  
		  }  
		
		return check;
    }


	/**Save the current grade file
	 * @param the list of grades to be saved
	 * @param fileName: the name with witch you want to save the file
	 */
	
	void saveFile(Heap allGrade, String fileName) {
		
		PrintWriter out = null;
		
		File file;
		String path = " ";
		
		try {
			out = new PrintWriter(fileName);
			file = new File(fileName);
			path = file.getAbsolutePath();
		}
		
		catch (FileNotFoundException exp) {
		///ERROR TO PRINT TO THE SCREEN
			System.out.println("Error file not found");
			exp.printStackTrace();
		}
	
		for(int i = 1; i <= allGrade.size; i++) {
			Grade grad = allGrade.gradeList[i];
			out.println(grad.getGrade());
		} 
		
		//To print to the SCREEN
		System.out.println("The file has been saved in this path: ");
		System.out.println(path);
		
		out.close();
	}
	
	
	/**Determined if it is A, B, C, F...
	 * @return the letter grade corresponding
	 * @param grade value to be analyze
	 */
	
	char gradeLetter(double grade){
		
		char letter;
		
		int whole = (int) grade;
		 double decimal = grade - whole;
		 
		 if (decimal >= 0.5) {
			 
			 grade = grade + 0.5;
		 }
		
		if(grade >= cutoffA)
			letter = 'A';
		
		else if(grade >= cutoffB)
			letter = 'B';
		
		else if(grade >= cutoffC)
			letter = 'C';
		
		else if(grade >= cutoffD)
			letter = 'D';
		
		else
			letter = 'F';
		
		return letter;
	}
	
	
	/**
	 * @param lowerBound the lowerBound to set
	 */
	 
	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}

	
	/**
	 * @param upperBound the upperBound to set
	 */
	 
	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}

	
	/** verify if the input is between the grade range before using it
	 * @param grade value to be check
	 * @return true if the grade value is valid and false otherwise
	 */
	
	boolean checkGradeRange(double value) {
		
		boolean check = false;
		
		if(value >= lowerBound && value <= upperBound && value >= 0)
			check = true;
		
		return check;
	}
	
	
	/**
	 * Function that find the sum
	 * @return sum of all grade
	 * @param list of grade
	 */
	
	private double sum(Heap allGrade) {
		
		double sum = 0;
		
		for(int index = 1; index <= allGrade.size; index++) {
			
			Grade grad = allGrade.gradeList[index];
			sum = sum + grad.getGrade();	
		} 
		
		return sum;
	}
	
	
	/**
	 * Function that count the total number of A, B, C, D, and F 
	 * and update the private variable counts
	 * @param list of grades
	 */
	
	void gradeDistribution  (Heap allGrade) {
		
		double grade;
		int whole;
		double decimal;	
			
		for(int index = 1; index <= allGrade.size; index++) {
			
			 Grade grad = allGrade.gradeList[index];
			 grade = grad.getGrade();
			 
			 whole = (int) grade;
			 decimal = grade - whole;
			 
			 if (decimal >= 0.5) {
				 
				 grade = grade + 0.5;
			 }
			
			if(grade >= cutoffA)
				numberOfA++;
			
			else if(grade >= cutoffB)
				numberOfB++;
			
			else if(grade >= cutoffC)
				numberOfC++;
			
			else if(grade >= cutoffD)
				numberOfD++;
			
			else 
				numberOfF++;
		} 
	}
		
	
	/**
	 * @return the numberOfA
	 */
	 
	public int getNumberOfA() {
		return numberOfA;
	}


	/**
	 * @return the numberOfB
	 */
	 
	public int getNumberOfB() {
		return numberOfB;
	}

	
	/**
	 * @return the numberOfC
	 */
	 
	public int getNumberOfC() {
		return numberOfC;
	}


	/**
	 * @return the numberOfD
	 */
	public int getNumberOfD() {
		return numberOfD;
	}


	/**
	 * @return the numberOfF
	 */
	 
	public int getNumberOfF() {
		return numberOfF;
	}


	/**
	 * @param numberOfF the numberOfF to set
	 */
	 
	public void setNumberOfF(int numberOfF) {
		this.numberOfF = numberOfF;
	}


	/**
	 * Function that computes median
	 * @return median of all grades
	 * @param list of grades
	 */
	
	double median(Heap allGrade) {
		
		int index;
		double median;
		
		boolean isEven = false;
		
		if(allGrade.size % 2 == 0) {
			
			index = allGrade.size / 2 ;
			isEven = true;
		}
		else
			index = 1 + allGrade.size / 2 ;
		
		Grade grad = allGrade.gradeList[index];
		
		if(isEven) {
			
			Grade grad2 = allGrade.gradeList[index + 1];
			
			median = 0.5 * ( grad.getGrade() + grad2.getGrade() );
		}
		else
			median = grad.getGrade();
		
		return median;
	}
	
	
	/**
	 * Function that computes average
	 * @return average of all grade
	 * @param list of grades
	 */
	
	double computeAverage(Heap allGrade) {
		
		double sum = sum(allGrade);
		
		return ( sum / allGrade.size );
	}
	

	/**
	 * get the minimun grade
	 * @param list representing all grade
	 * @return minimum grade of the list
	 */
	
	double minimunGrade (Heap list) {
		
		return list.gradeList[1].getGrade();
		
	}
	
	
	/**
	 * get the maximun grade
	 * @param list representing all grade
	 * @return maximun grade of the list
	 */
	
	double maximunGrade (Heap list) {
		
		return list.gradeList[list.size].getGrade();
		
	}
	
	
	/**
	 * Function that sort the grades in the list
	 * @param list of grade
	 */
	
	void sortGradeList(Heap allGrade) {
		
		buildHeap(allGrade);
		
		int size = allGrade.size;
		
		for(int index = allGrade.size; index > 1; index--) {
			
			Grade temp = new Grade();
			temp = allGrade.gradeList[1];
			allGrade.gradeList[1] = allGrade.gradeList[index];
			allGrade.gradeList[index] = temp;
			
			allGrade.size--;
	
			heapify (allGrade, 1);
		}
		
		allGrade.size = size;
	}
	
	
	/**
	 * delete a grade
	 * @param list representing all grade
	 * @param the index of the grade to be delete
	 */
	
	void deleteGrade (Heap list, double grade) {
		
		if(list.size < 1) {
			
			//PRINT OUT TO THE SCREEN
			System.out.println("Error! No grade in the list");
		}	
		else {
			
			boolean found = false;
			
			for(int index = 1; index <= list.size; index++) {
				
				Grade grad = list.gradeList[index];
				
				if(grad.getGrade() == grade) {
					
					found = true;
					
					list.gradeList[index] = list.gradeList[list.size];
					list.size--;
					
					heapify(list, index);
					
					index = list.size + 1;
				}			
			}
			
			if(!found) {
				
				//PRINT OUT TO THE SCREEN
				System.out.println("Error! this grade is not in the current list");
			}
		}
	}
	
	
	/**
	 * replace a grade in the list
	 * @param list representing all grade
	 * @param the old grade to be replace
	 * @param newGrade, the new grade
	 */

	 void replaceGrade (Heap list, double oldGrade, double newGrade) {
		
		 boolean found = false;
			
		 for(int index = 1; index <= list.size; index++) {
			
			Grade grad = list.gradeList[index];
			
			if(grad.getGrade() == oldGrade) {
				
				found = true;
				
				list.gradeList[index].setGrade(newGrade);
				
				buildHeap(list);
				
				index = list.size + 1;
			}			
		}
		
		if(!found) {
			
			//PRINT OUT TO THE SCREEN
			System.out.println("Error! this grade is not in the current list");
		}
	 }
	 
	 
	/**
	 * insert an element in the list
	 * @param list representing all grade
	 * @param key, the grade to be inserted
	 */
	
	void insertGrade(Heap list, String input) {		
		
		list.size++;
		
		Double grade;
		int size;
		
		if(list.size == list.capacity) {
			
			list.capacity = expandCapacity(list.capacity);
		}
		
		size = list.size;
		
			grade = Double.parseDouble(input);
		
			while (size > 1 && list.gradeList[size / 2].getGrade() < grade){
				 
				list.gradeList[size] = list.gradeList[parent (size)];
				size = parent (size);
			}
			
			Grade grad = new Grade(grade);
			
			list.gradeList[size] = grad;
		
	}
		
	
	/**
	 * build the heap data structure
	 * @param list representing all grade
	 */
	
	void buildHeap(Heap list) {
		
		for (int index = list.getSize() / 2; index > 0 ; index--) {
			
			heapify (list, index);
		}
	}
	
	
	/**
	 * heapify of an element
	 * @param current index in the tree 
	 * @param list representing all grade
	 */
	                                           
	void heapify(Heap list, int index){
		
		int largest;// = index;
		int left = index * 2;
		int right = ( index * 2 ) + 1;
		
		Grade grade1 = list.gradeList[left];
		Grade grade2 = list.gradeList[index];
		Grade grade3 = list.gradeList[right];
		
		//System.out.println("grade1 " + grade1.getGrade());
		
		if(left <= list.getSize() &&
			grade1.getGrade() > grade2.getGrade())
			
			largest = left;
		else
			largest = index;
		
		Grade grade4 = list.gradeList[largest];
		
		if(right <= list.getSize() &&
				grade3.getGrade() > grade4.getGrade())
				
				largest = right;
		
		if (largest != index) {
			
			//swap(list.gradeList[index] , list.gradeList[largest] );
			//exchange the element at largest and element at index
			Grade temp = new Grade();
			temp = list.gradeList[index];
			list.gradeList[index] = list.gradeList[largest];
			list.gradeList[largest] = temp;
			
			heapify (list, largest);
		}
			
	}
	

	/**Function that return a new capacity greater than the current one
	 * @param current capacity
	 * @return a new expanded capacity
	 */

	int expandCapacity(int currentCapacity) {
		int Newcapacity = 1;
		int power = 1;

		//get the close power of 2 greater than the size
		while (Newcapacity <= currentCapacity) {

			Newcapacity = (int) Math.pow(2.0, power); 

			power++; 

		}

		return Newcapacity; //return the new capacity to the caller

	}
	
	
	/**
	 * parent of an element
	 * @return parent of index in the tree
	 */

	int parent(int index) {
		
		return index / 2;
	}
	
}
