//package gradeAnal;

/**Class that represents a grade
 *CSE 360 Summer 2019
 *@ author: Team 6
 * Team 6 members: Jared Krause, Noor Attiah, Larissa Pokam  
 */

public class Grade {
	
	private double grade;

	/**default constuctor
	 * @param grade
	 * 
	 */
	
	public Grade() {
		
		grade = 0;
	}
	
	/**constructor 
	 * @param grade
	 */
	 
	public Grade(double grade) {
		
		this.grade = grade;
	}

	
	/**get a grade value
	 * @param no parameter
	 * @return the current grade
	 */
	
	public double getGrade() {
		return grade;
	}

	
	/** set a grade value
	 * @param grade
	 * @return no return value
	 */
	
	public void setGrade(double grade) {
		this.grade = grade;
	}

	/** to print the grade value 
	 * @param no parameter
	 * @return the grade value as a string
	 */
	 
	public String toString() {
		return "Grade [grade=" + grade + "]";
	}

}