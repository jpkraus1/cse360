//package gradeAnal;
/**Class that represents the heap data structure that holds grade
 *CSE 360 Summer 2019
 *@ author: Team 6
 * Team 6 members: Jared Krause, Noor Attiah, Larissa Pokam  
 */
 
import java.util.Arrays;

public class Heap {
	
	protected int size;
	protected int capacity;
	protected Grade gradeList [];
		
	
	/**
	 * default constructor
	 */
	 
	public Heap() {
		size = 0;
		capacity = 0;
		gradeList = null;
	}


	/**Initialized attributes
	 * @param capacity
	 */
	 
	public Heap(int capacity) {
		
		size = 0;
		this.capacity = capacity;
		gradeList = new Grade [capacity];
	}
	

	/**
	 * @return the size
	 */
	 
	public int getSize() {
		return size;
	}
	

	/**
	 * @param size the size to set
	 */
	 
	public void setSize(int size) {
		this.size = size;
	}


	/**
	 * @return the capacity
	 */
	 
	public int getCapacity() {
		return capacity;
	}


	/**
	 * @param capacity the capacity to set
	 */
	 
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	
	@Override
	public String toString() {
		return "Heap [size=" + size + ", capacity=" + capacity + ", gradeList=" + Arrays.toString(gradeList) + "]";
	}
	 
}